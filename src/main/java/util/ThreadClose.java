package util;

import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ThreadClose extends Thread{
     List<Socket> socketsList = new ArrayList<Socket>();
    public ThreadClose(List<Socket> socketsList) {
        this.socketsList = socketsList;
    }

    @Override
    public void run() {
        System.out.println("---  System out ------  " +socketsList.size());
        for (Socket socketToClose : socketsList ) {
            if (!socketToClose.isClosed()){
                try {
                    socketToClose.shutdownOutput();
//                    socketToClose.getInputStream().read();
                    socketToClose.close();
                    System.out.println("---  close socket ------   "+socketToClose.getRemoteSocketAddress()+":"+socketToClose.getPort());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
