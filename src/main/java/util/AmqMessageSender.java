package util;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.jms.*;

/**
 * Created by Administrator on 2017/10/6.
 */
public  class AmqMessageSender {
    private static Log log = LogFactory.getLog(AmqMessageSender.class);
        static ConnectionFactory connectionFactory;
        static Connection connection = null;
        static Session session;
        static Destination destination;
        static MessageProducer producer;
        static TextMessage message = null;
    public static void sendMessage(String msg_url, String msg_name_GCN, String jsonMessage) {
        connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER
                , ActiveMQConnection.DEFAULT_PASSWORD, msg_url);
        try {
            log.debug("send AMQ try" );
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
            destination = session.createTopic(msg_name_GCN);
//            destination = session.createQueue(msg_name_GCN);
            producer = session.createProducer(destination);
            //todo use json text message
            message=session.createTextMessage(jsonMessage);
            producer.send(message);
            session.commit();
            log.debug("send AMQ " + jsonMessage);
        } catch (JMSException e) {
            log.debug("send AMQ Error "  +e.getMessage());
            e.printStackTrace();
        }finally {
            log.debug("send AMQ finally" );
            try {
                if (null != connection){
                    connection.close();
                }
            } catch (Throwable ignore) {
            }
        }
    }
}
