package gcn;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
//import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.List;
//import java.util.stream.Stream;

import org.apache.commons.configuration2.AbstractConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;

import util.XML2File;

public class GSN_ClientTestACP {
    public static void main(String[] args) {
        try {

        	Configurations configs = new Configurations();
        	Configuration config = null;
        	
        	try
        	{
        	    config = configs.properties(new File("usergui.properties"));
        	}
        	catch (ConfigurationException cex)
        	{
        	    // Something went wrong
        		XML2File.writeToLog(cex.getMessage());
        		cex.printStackTrace();
        	}
//        	SimpleDateFormat shortDateFormat = new SimpleDateFormat("yyyyMMdd");
//        	SimpleDateFormat longDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        	((AbstractConfiguration) config).setListDelimiterHandler(new DefaultListDelimiterHandler(','));
         	int  SERVER_PORT = config.getInt("SERVER_PORT" , 5348);
         	String SERVER_IP = config.getString("SERVER_IP" , "127.0.0.1");
        	System.out.println("SERVER IP PORT : "+SERVER_IP+"   " +SERVER_PORT);
            Socket socket = new Socket(SERVER_IP, SERVER_PORT);
           
//            socket.setSoTimeout(600000);
            socket.setSoTimeout(6000);
            socket.setSoLinger(true,5);
//            socket.getInputStream().read;
//            socket.getOutputStream().write(new byte[]{1,2});


            PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);



            //这里要多加测试： 生成普通plan  BAT plan  BAT 不同方位，不同高度 的plan
            
            String testFilePath1=config.getString("testFilePath1",new File(new File("").getAbsolutePath(), "testFilePath1.xml").getPath());
            String testFilePath2=config.getString("testFilePath2",new File(new File("").getAbsolutePath(), "testFilePath2.xml").getPath());
            String testFilePath3=config.getString("testFilePath3",new File(new File("").getAbsolutePath(), "testFilePath3.xml").getPath());
            String testFilePath4=config.getString("testFilePath4",new File(new File("").getAbsolutePath(), "testFilePath4.xml").getPath());
            String testFilePath5=config.getString("testFilePath5",new File(new File("").getAbsolutePath(), "testFilePath5.xml").getPath());
            String testFilePath6=config.getString("testFilePath6",new File(new File("").getAbsolutePath(), "testFilePath6.xml").getPath());
            String testFilePath7=config.getString("testFilePath7",new File(new File("").getAbsolutePath(), "testFilePath7.xml").getPath());
            String testFilePath8=config.getString("testFilePath8",new File(new File("").getAbsolutePath(), "testFilePath8.xml").getPath());
            String testFilePath9=config.getString("testFilePath9",new File(new File("").getAbsolutePath(), "testFilePath9.xml").getPath());
            String testFilePath10=config.getString("testFilePath10",new File(new File("").getAbsolutePath(), "testFilePath10.xml").getPath());
            String testFilePath11=config.getString("testFilePath11",new File(new File("").getAbsolutePath(), "testFilePath11.xml").getPath());
            String testFilePath12=config.getString("testFilePath12",new File(new File("").getAbsolutePath(), "testFilePath12.xml").getPath());
            String testFilePath13=config.getString("testFilePath13",new File(new File("").getAbsolutePath(), "testFilePath13.xml").getPath());
            String testFilePath14=config.getString("testFilePath14",new File(new File("").getAbsolutePath(), "testFilePath14.xml").getPath());
            String testFilePath15=config.getString("testFilePath15",new File(new File("").getAbsolutePath(), "testFilePath15.xml").getPath());
            List<String> filePathList=new ArrayList<String>();
            filePathList.add(testFilePath1);
            filePathList.add(testFilePath2);
            filePathList.add(testFilePath3);
            filePathList.add(testFilePath4);
            filePathList.add(testFilePath5);
            filePathList.add(testFilePath6);
            filePathList.add(testFilePath7);
            filePathList.add(testFilePath8);
            filePathList.add(testFilePath9);
            filePathList.add(testFilePath10);
            filePathList.add(testFilePath11);
            filePathList.add(testFilePath12);
            filePathList.add(testFilePath13);
            filePathList.add(testFilePath14);
            filePathList.add(testFilePath15);
            while (socket.isConnected() ) {
//            	socket.getInputStream().read();
            	System.out.println("isClosed = " +socket.isClosed());
            	System.out.println("isConnected = " +socket.isConnected());
            	System.out.println("isBound = " +socket.isBound());
            	System.out.println("isInputShutdown = " +socket.isInputShutdown());
            	System.out.println("isOutputShutdown = " +socket.isOutputShutdown());
            	System.out.println("RemoteSocketAddress = " +socket.getRemoteSocketAddress());
            	System.out.println("getLocalAddress = " +  socket.getLocalSocketAddress());

                printWriter = new PrintWriter(socket.getOutputStream(), true);
            	BufferedReader sysBuff = new BufferedReader(new InputStreamReader(System.in));
//            	printWriter.println(sysBuff.readLine());
            	String s =sysBuff.readLine();
            	String toPrint=XML2File.readLogToString(filePathList.get(Integer.parseInt(s)-1));
            	printWriter.print(toPrint);
            	System.out.println(toPrint);
//            	printWriter.println(testXml.get(Integer.parseInt(s)-1));
            	// 刷新输出流，使Server马上收到该字符串
            	printWriter.flush();
//            	String result = bufferedReader.readLine()+"\r\n";
//            	if (result.getBytes()[0]==0) {
////            	bufferedReader.skip(4); //服务器发来的每个包消息头部有字符   要查一下原因
//					result=result.substring(result.indexOf('<'));
//				}
//            	XML2File.writeLog(result, "D:\\GCN_log.txt");
//            	System.out.println("Server say : " + result);
//            	System.out.println("Server say : " + BinaryToHexString(result.getBytes()));

            	if (false) {
					break;
				}
			}

            /** 关闭Socket*/
            printWriter.close();
//            bufferedReader.close();
            socket.close();
        } catch (Exception e) {
            System.out.println("Exception:" + e);
        }
    }
    public static String BinaryToHexString(byte[] bytes){
    	
    	String hexStr =  "0123456789ABCDEF";
    	String result = "";  
    	String hex = "";  
    	for(int i=0;i<bytes.length;i++){  
    		//字节高4位  
    		hex = String.valueOf(hexStr.charAt((bytes[i]&0xF0)>>4));  
    		//字节低4位  
    		hex += String.valueOf(hexStr.charAt(bytes[i]&0x0F));  
    		result +=hex+" ";  //这里可以去掉空格，或者添加0x标识符。
    	}  
    	return result;  
    }
}